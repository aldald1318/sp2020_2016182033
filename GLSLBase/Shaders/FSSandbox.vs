#version 450 // 버전

in vec3 a_Position; //vs input 선언, float 3개, in -> vs input->attrib

out vec2 v_TexCoord;

// params
// float c_PI = 3.141592;

void main()
{
	float newX =  a_Position.x +0.5;
	float newY =  a_Position.y +0.5;

	v_TexCoord = vec2(newX, newY);

	vec3 pos = a_Position;
	pos *= 2;
	gl_Position = vec4(pos, 1.f);
}
