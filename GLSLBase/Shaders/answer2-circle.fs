#version 450

layout(location=0) out vec4 FragColor;

in vec2 out_UV;

void main()
{
	float distance0 = distance(out_UV,vec2(0.5,0.5));

	if(distance0 < 0.4)
		FragColor = vec4(0);
	else
		FragColor = vec4(1);
}
