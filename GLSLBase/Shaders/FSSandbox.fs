#version 450

layout(location=0) out vec4 FragColor;

in vec2 v_TexCoord;

float c_PI = 3.141592;

uniform float u_Time = 0.0;
uniform vec2 u_InputPoints[10]; 
// 특정한 좌표
// 이 모든 좌표에 대해 모두 원을 그려라

void main()
{
   // 대상 설치
   vec2 inputPoint = vec2(0.75, 0.75);
   float inputPointSize = 0.05;

   int isCircleLineRegion = 0;

   // 라인을 그리기 위한 거리구하기: 반지름 0.5
   float distance1 = distance(v_TexCoord, vec2(0.5,0.5));
   
   // 회전하는지의 대한 정보구하기
   float radian = atan(v_TexCoord.y-0.5, v_TexCoord.x-0.5) + c_PI; // 0 ~ 2PI
   float tmpTime = mod(u_Time, c_PI * 2); // 나머지 연산

   // 회전하는 선 그려주기
   if(radian > tmpTime - 0.01 && radian < tmpTime && distance1 <= 0.5)
   {
      FragColor = vec4(0.7);
      isCircleLineRegion = 1;
   }
   else
      FragColor = vec4(1);

   // 회전하는 범위에 포함된 물체 그려주기
   if(isCircleLineRegion > 0)
   {
      float distance0 = distance(v_TexCoord, inputPoint);
      if( distance0 < inputPointSize)
      {
         FragColor = vec4(1.0);
      }
   }

}
