#version 450

layout(location=0) out vec4 FragColor;

const float PI = 3.141592;
in vec2 out_UV;

void main()
{
	float wave = sin(2*PI *out_UV.x);
	wave = (wave+1)/2.0;

	if(out_UV.y < wave)
		FragColor = vec4(0);
	else
		FragColor = vec4(1);
}	
