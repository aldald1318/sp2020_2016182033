#version 450 // ����

in vec3 a_Position;
in float a_Value;
in vec2 a_UV; 
uniform float u_Time;
const float PI = 3.141592;

void main()
{
	float speed = 0.1;

	float newPosX = a_Position.x + (fract(u_Time*speed)*2 - 1.0);
	float newPosY = a_Position.y + sin(fract(u_Time*speed)*2*PI);
	gl_Position = vec4(newPosX, newPosY, 0, 1);
}
