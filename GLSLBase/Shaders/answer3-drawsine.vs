#version 450 // ����

in vec3 a_Position;
in float a_Value;
in vec2 a_UV; 

out vec2 out_UV;

uniform float u_Time;
const float PI = 3.141592;

void main()
{
	out_UV = a_UV;

	vec3 pos = a_Position;
	pos *= 10;

	gl_Position = vec4(pos, 1);
}
