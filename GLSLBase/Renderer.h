#pragma once

#include <string>
#include <cstdlib>
#include <fstream>
#include <iostream>

#include "Dependencies\glew.h"
#include "Dependencies\wglew.h"
#include "Dependencies\glm/glm.hpp"
#include "Dependencies\glm/gtc/matrix_transform.hpp"
#include "Dependencies\glm/gtx/euler_angles.hpp"

class Renderer
{
public:
	Renderer(int windowSizeX, int windowSizeY);
	~Renderer();

	GLuint CreatePngTexture(char * filePath);
	GLuint CreateBmpTexture(char * filePath);
	   
	void Test();
	void DrawSign();
	void FSSandbox(float elapsedTime);
	
///////////////////////////////////////
// Final Test
public:
	void DrawAnswer1234(float elapsedTime, GLuint shader);
	void Lecture3(float elapsedTime);

private:
	void CreateVBOFinalTest();
	void CreateParticle(int count);

public:
	GLuint m_Shader_Answer1_Sine = 0;
	GLuint m_Shader_Answer2_Circle = 0;
	GLuint m_Shader_Answer3_DrawSine = 0;
	GLuint m_Shader_Answer4_Rader = 0;

	GLuint m_VBO_Answer1_Sine = 0;

	float m_TotalTime_Answer1_Sine = 0;

/////////////////////////////////////////

private:
	void Initialize(int windowSizeX, int windowSizeY);
	bool ReadFile(char* filename, std::string *target);
	void AddShader(GLuint ShaderProgram, const char* pShaderText, GLenum ShaderType);
	GLuint CompileShaders(char* filenameVS, char* filenameFS);
	void CreateVertexBufferObjects(); 
	unsigned char * Renderer::loadBMPRaw(const char * imagepath, unsigned int& outWidth, unsigned int& outHeight);

	bool m_Initialized = false;
	
	unsigned int m_WindowSizeX = 0;
	unsigned int m_WindowSizeY = 0;

	GLuint m_SolidRectShader = 0;
	GLuint m_FSSandboxShader = 0;

	GLuint m_VBORect = 0;

	GLuint m_VBOTest = 0;
	GLuint m_VBOTestColor = 0;

	GLuint m_VBOSingleParticle = 0;

	GLuint m_VBOManyParticle = 0;
	GLuint m_VBOManyParticleCount = 0;

	GLuint m_VBOFSSandbox = 0;

	// Initial Name
	GLuint m_VBOInitialName = 0;

	float m_TotalTimeLecture3 = 0;
	float m_TotalTimeFSSandBox = 0;
};

