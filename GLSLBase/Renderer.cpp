#include "stdafx.h"
#include "Renderer.h"
#include "LoadPng.h"
#include <Windows.h>
#include <cstdlib>
#include <cassert>

Renderer::Renderer(int windowSizeX, int windowSizeY)
{
	Initialize(windowSizeX, windowSizeY);
}


Renderer::~Renderer()
{
}

////////////////////////// 기말고사 답 ///////////////////////////

void Renderer::CreateVBOFinalTest()
{
	float size = 0.05f;
	float rect[]
		=
	{
		-size, -size, 0.f,	0.5f,	0.f, 0.f,//x, y, z, value, u, v
		-size, size, 0.f,	0.5f,	0.f, 1.f,
		size, size, 0.f,	0.5f,	1.f, 1.f, //Triangle1
		-size, -size, 0.f,	0.5f,	0.f, 0.f,
		size, size, 0.f,	0.5f,	1.f, 1.f,
		size, -size, 0.f,	0.5f,	1.f, 0.f, //Triangle2	

		//- size, -size, 0.f, 1.f, 0.f, 0.f,
		//-size, size, 0.f, 1.f, 0.f, 1.f,
		//size, size, 0.f, 1.f, 1.f, 1.f, //Triangle3
		//-size, -size, 0.f, 1.f, 0.f, 0.f,
		//size, size, 0.f, 1.f, 1.f, 1.f,
		//size, -size, 0.f, 1.f, 1.f, 0.f //Triangle4

	};

	glGenBuffers(1, &m_VBO_Answer1_Sine);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO_Answer1_Sine);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rect), rect, GL_STATIC_DRAW);
}

void Renderer::DrawAnswer1234(float elapsedTime, GLuint shader)
{
	int vertexStride = 6; // position(3), value(1), uv(2)
	int vertexCount = 6; // Triangle 4개

	glUseProgram(shader);

	static float g_sTime = 0.f; g_sTime += 0.01;

	GLuint uTime = glGetUniformLocation(shader, "u_Time");
	glUniform1f(uTime, g_sTime);

	GLuint aPos = glGetAttribLocation(shader, "a_Position");
	GLuint aValue = glGetAttribLocation(shader, "a_Value");
	GLuint aUV = glGetAttribLocation(shader, "a_UV");

	glEnableVertexAttribArray(aPos);
	glEnableVertexAttribArray(aValue);
	glEnableVertexAttribArray(aUV);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBO_Answer1_Sine);
	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * vertexStride, 0);
	glVertexAttribPointer(aValue, 1, GL_FLOAT, GL_FALSE, sizeof(float) * vertexStride, (GLvoid*)(sizeof(float) * 3));
	glVertexAttribPointer(aUV, 2, GL_FLOAT, GL_FALSE, sizeof(float) * vertexStride, (GLvoid*)(sizeof(float) * 4));
	glDrawArrays(GL_TRIANGLES, 0, vertexCount);
}


void Renderer::Initialize(int windowSizeX, int windowSizeY)
{
	//Set window size
	m_WindowSizeX = windowSizeX;
	m_WindowSizeY = windowSizeY;

	//Load shaders
	m_SolidRectShader = CompileShaders("./Shaders/SolidRect.vs", "./Shaders/SolidRect.fs");
	m_FSSandboxShader = CompileShaders("./Shaders/FSSandbox.vs", "./Shaders/FSSandbox.fs");
	
	//Create VBOs
	CreateVertexBufferObjects();

	// 기말고사
	m_Shader_Answer1_Sine = CompileShaders("./Shaders/answer1-sine.vs", "./Shaders/answer1-sine.fs");
	m_Shader_Answer2_Circle = CompileShaders("./Shaders/answer2-circle.vs", "./Shaders/answer2-circle.fs");
	m_Shader_Answer3_DrawSine = CompileShaders("./Shaders/answer3-drawsine.vs", "./Shaders/answer3-drawsine.fs");
	m_Shader_Answer4_Rader = CompileShaders("./Shaders/answer4-rader.vs", "./Shaders/answer4-rader.fs");

	CreateVBOFinalTest();
}

void Renderer::CreateVertexBufferObjects()
{
	float rect[]
		=
	{
		-0.5, -0.5, 0.f, -0.5, 0.5, 0.f, 0.5, 0.5, 0.f, //Triangle1
		-0.5, -0.5, 0.f,  0.5, 0.5, 0.f, 0.5, -0.5, 0.f, //Triangle2
	};

	glGenBuffers(1, &m_VBORect);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBORect);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rect), rect, GL_STATIC_DRAW);

	float rectFSSandbox[]
		=
	{
		-0.5, -0.5, 0.f, -0.5, 0.5, 0.f, 0.5, 0.5, 0.f, //Triangle1
		-0.5, -0.5, 0.f,  0.5, 0.5, 0.f, 0.5, -0.5, 0.f, //Triangle2
	};

	glGenBuffers(1, &m_VBOFSSandbox);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOFSSandbox);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rectFSSandbox), rectFSSandbox, GL_STATIC_DRAW);

	float vertices[]
		=
	{
		0.f,0.f,0.f,	//v0
		1.f,0.f,0.f,	//v1
		1.f,1.f,0.f,	//v2
	}; // CPU memory에 있음 -> GPU memory로 옮겨야함

	glGenBuffers(1, &m_VBOTest); // just generate ID
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTest); // 용도 부여
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW); // CPU->GPU복사, bind된 vbo에 데이터를 할당

	float color[]
		=
	{
		1.f,0.f,0.f,1.f,	//v0
		0.f,1.f,0.f,1.f,	//v1
		0.f,0.f,1.f,1.f,	//v2
	}; // CPU memory에 있음 -> GPU memory로 옮겨야함

	glGenBuffers(1, &m_VBOTestColor); // just generate ID
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTestColor); // 용도 부여
	glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW); // CPU->GPU복사, bind된 vbo에 데이터를 할당

	// 파티클
	float particleSize = 0.01f;
	float singleParticleVertices[] // 삼각형 두개, 사각형 (vertex => 6)
		=
	{
		-1.f * particleSize, -1.f * particleSize, 0.f,	//v0
		 1.f * particleSize,  1.f * particleSize, 0.f,	//v1
		-1.f * particleSize,  1.f * particleSize, 0.f,	//v2
		-1.f * particleSize, -1.f * particleSize, 0.f,	//v3
		 1.f * particleSize, -1.f * particleSize, 0.f,	//v4
		 1.f * particleSize,  1.f * particleSize, 0.f,	//v5
	};

	glGenBuffers(1, &m_VBOSingleParticle); // just generate ID
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOSingleParticle); // 용도 부여
	glBufferData(GL_ARRAY_BUFFER, sizeof(singleParticleVertices), singleParticleVertices, GL_STATIC_DRAW); // CPU->GPU복사, bind된 vbo에 데이터를 할당

	// Name Initial
	float name[]
		=
	{
		// L
		-0.7,0.5,0,
		-0.7,-0.5,0,
		-0.4,-0.5,0,

		// W
		-0.3,0.5,0,
		-0.2,-0.5,0,
		0,0.3,0,
		0.2,-0.5,0,
		0.3,0.5,0,

		// T
		0.5,0.5,0,
		0.9,0.5,0,
		0.7,0.5,0,
		0.7,-0.5,0,

	}; // CPU memory에 있음 -> GPU memory로 옮겨야함

	glGenBuffers(1, &m_VBOInitialName); // just generate ID
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOInitialName); // 용도 부여
	glBufferData(GL_ARRAY_BUFFER, sizeof(name), name, GL_STATIC_DRAW); // CPU->GPU복사, bind된 vbo에 데이터를 할당

	// Create Many Particles
	CreateParticle(10000);
}

void Renderer::CreateParticle(int count)
{
	int InputLayoutCount = 15; //+4 rgba
	float* particleVertices = new float[count * InputLayoutCount * 3 * 2]; // 2 triangles 3 vertices 7 floats (pos(x,y,z), vel(vx,vy,vz), startTime)
	int floatCount = count * InputLayoutCount * 3 * 2;
	int vertexCount = count * 3 * 2;
	
	int index = 0;
	float particleSize = 0.01f;

	for (int i = 0; i < count; ++i)
	{
		// quad -> 1 particle -> vertex should have same velocity

		float randomValueX = 0.f;
		float randomValueY = 0.f;
		float randomValueZ = 0.f;
		float randomValueVX = 0.f;
		float randomValueVY = 0.f;
		float randomValueVZ = 0.f;
		float randomStartTime = 0.f;
		float randomLifeTime = 0.f;
		float randomPeriod = 0.f;
		float randomAmp = 0.f;
		float randomValue = 0.f;
		float randomR, randomG, randomB, randomA = 0.f;

		randomValueX = 0; // (rand() / (float)RAND_MAX - 0.5) * 2.f;
		randomValueY = 0; // (rand() / (float)RAND_MAX - 0.5) * 2.f;
		randomValueZ = 0.f;

		randomValueVX = (rand() / (float)RAND_MAX - 0.5) * 0.5f;
		randomValueVY = (rand() / (float)RAND_MAX - 0.5) * 0.5f;
		randomValueVZ = 0.f;

		randomStartTime = (rand() / (float)RAND_MAX) * 6.f;
		randomLifeTime = (rand() / (float)RAND_MAX) * 3.f;

		randomPeriod = (rand() / (float)RAND_MAX) * 3.f;
		randomAmp = 0.f; // (rand() / (float)RAND_MAX) * 0.1f;

		randomValue = (rand() / (float)RAND_MAX) * 1.f;

		randomR = (rand() / (float)RAND_MAX);
		randomG = (rand() / (float)RAND_MAX);
		randomB = (rand() / (float)RAND_MAX);
		randomA = (rand() / (float)RAND_MAX);

		// v0
		particleVertices[index] = -particleSize / 2.f + randomValueX;
		index++;
		particleVertices[index] = -particleSize / 2.f + randomValueY;
		index++;
		particleVertices[index] = 0.f;
		index++;
		particleVertices[index] = randomValueVX;
		index++;
		particleVertices[index] = randomValueVY;
		index++;
		particleVertices[index] = randomValueVZ;
		index++;
		particleVertices[index] = randomStartTime;
		index++;
		particleVertices[index] = randomLifeTime;
		index++;
		particleVertices[index] = randomPeriod;
		index++;
		particleVertices[index] = randomAmp;
		index++;
		particleVertices[index] = randomValue;
		index++;
		particleVertices[index] = randomR;
		index++;
		particleVertices[index] = randomG;
		index++;
		particleVertices[index] = randomB;
		index++;
		particleVertices[index] = randomA;
		index++;

		// v1
		particleVertices[index] = particleSize / 2.f + randomValueX;
		index++;
		particleVertices[index] = -particleSize / 2.f + randomValueY;
		index++;
		particleVertices[index] = 0.f;
		index++;
		particleVertices[index] = randomValueVX;
		index++;
		particleVertices[index] = randomValueVY;
		index++;
		particleVertices[index] = randomValueVZ;
		index++;
		particleVertices[index] = randomStartTime;
		index++;
		particleVertices[index] = randomLifeTime;
		index++;
		particleVertices[index] = randomPeriod;
		index++;
		particleVertices[index] = randomAmp;
		index++;
		particleVertices[index] = randomValue;
		index++;
		particleVertices[index] = randomR;
		index++;
		particleVertices[index] = randomG;
		index++;
		particleVertices[index] = randomB;
		index++;
		particleVertices[index] = randomA;
		index++;

		// v2
		particleVertices[index] = particleSize / 2.f + randomValueX;
		index++;
		particleVertices[index] = particleSize / 2.f + randomValueY;
		index++;
		particleVertices[index] = 0.f;
		index++;
		particleVertices[index] = randomValueVX;
		index++;
		particleVertices[index] = randomValueVY;
		index++;
		particleVertices[index] = randomValueVZ;
		index++;
		particleVertices[index] = randomStartTime;
		index++;
		particleVertices[index] = randomLifeTime;
		index++;
		particleVertices[index] = randomPeriod;
		index++;
		particleVertices[index] = randomAmp;
		index++;
		particleVertices[index] = randomValue;
		index++;
		particleVertices[index] = randomR;
		index++;
		particleVertices[index] = randomG;
		index++;
		particleVertices[index] = randomB;
		index++;
		particleVertices[index] = randomA;
		index++;

		// v3
		particleVertices[index] = -particleSize / 2.f + randomValueX;
		index++;
		particleVertices[index] = -particleSize / 2.f + randomValueY;
		index++;
		particleVertices[index] = 0.f;
		index++;
		particleVertices[index] = randomValueVX;
		index++;
		particleVertices[index] = randomValueVY;
		index++;
		particleVertices[index] = randomValueVZ;
		index++;
		particleVertices[index] = randomStartTime;
		index++;
		particleVertices[index] = randomLifeTime;
		index++;
		particleVertices[index] = randomPeriod;
		index++;
		particleVertices[index] = randomAmp;
		index++;
		particleVertices[index] = randomValue;
		index++;
		particleVertices[index] = randomR;
		index++;
		particleVertices[index] = randomG;
		index++;
		particleVertices[index] = randomB;
		index++;
		particleVertices[index] = randomA;
		index++;

		// v4
		particleVertices[index] = particleSize / 2.f + randomValueX;
		index++;
		particleVertices[index] = particleSize / 2.f + randomValueY;
		index++;
		particleVertices[index] = 0.f;
		index++;
		particleVertices[index] = randomValueVX;
		index++;
		particleVertices[index] = randomValueVY;
		index++;
		particleVertices[index] = randomValueVZ;
		index++;
		particleVertices[index] = randomStartTime;
		index++;
		particleVertices[index] = randomLifeTime;
		index++;
		particleVertices[index] = randomPeriod;
		index++;
		particleVertices[index] = randomAmp;
		index++;
		particleVertices[index] = randomValue;
		index++;
		particleVertices[index] = randomR;
		index++;
		particleVertices[index] = randomG;
		index++;
		particleVertices[index] = randomB;
		index++;
		particleVertices[index] = randomA;
		index++;

		// v5
		particleVertices[index] = -particleSize / 2.f + randomValueX;
		index++;
		particleVertices[index] = particleSize / 2.f + randomValueY;
		index++;
		particleVertices[index] = 0.f;
		index++;
		particleVertices[index] = randomValueVX;
		index++;
		particleVertices[index] = randomValueVY;
		index++;
		particleVertices[index] = randomValueVZ;
		index++;
		particleVertices[index] = randomStartTime;
		index++;
		particleVertices[index] = randomLifeTime;
		index++;
		particleVertices[index] = randomPeriod;
		index++;
		particleVertices[index] = randomAmp;
		index++;
		particleVertices[index] = randomValue;
		index++;
		particleVertices[index] = randomR;
		index++;
		particleVertices[index] = randomG;
		index++;
		particleVertices[index] = randomB;
		index++;
		particleVertices[index] = randomA;
		index++;
	}

	glGenBuffers(1, &m_VBOManyParticle);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOManyParticle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * floatCount, particleVertices, GL_STATIC_DRAW);
	m_VBOManyParticleCount = vertexCount;
}

void Renderer::AddShader(GLuint ShaderProgram, const char* pShaderText, GLenum ShaderType)
{
	//쉐이더 오브젝트 생성
	GLuint ShaderObj = glCreateShader(ShaderType);

	if (ShaderObj == 0) {
		fprintf(stderr, "Error creating shader type %d\n", ShaderType);
	}

	const GLchar* p[1];
	p[0] = pShaderText;
	GLint Lengths[1];
	Lengths[0] = (GLint)strlen(pShaderText);
	//쉐이더 코드를 쉐이더 오브젝트에 할당
	glShaderSource(ShaderObj, 1, p, Lengths);

	//할당된 쉐이더 코드를 컴파일
	glCompileShader(ShaderObj);

	GLint success;
	// ShaderObj 가 성공적으로 컴파일 되었는지 확인
	glGetShaderiv(ShaderObj, GL_COMPILE_STATUS, &success);
	if (!success) {
		GLchar InfoLog[1024];

		//OpenGL 의 shader log 데이터를 가져옴
		glGetShaderInfoLog(ShaderObj, 1024, NULL, InfoLog);
		fprintf(stderr, "Error compiling shader type %d: '%s'\n", ShaderType, InfoLog);
		printf("%s \n", pShaderText);
	}

	// ShaderProgram 에 attach!!
	glAttachShader(ShaderProgram, ShaderObj);
}

bool Renderer::ReadFile(char* filename, std::string *target)
{
	std::ifstream file(filename);
	if (file.fail())
	{
		std::cout << filename << " file loading failed.. \n";
		file.close();
		return false;
	}
	std::string line;
	while (getline(file, line)) {
		target->append(line.c_str());
		target->append("\n");
	}
	return true;
}

GLuint Renderer::CompileShaders(char* filenameVS, char* filenameFS)
{
	GLuint ShaderProgram = glCreateProgram(); //빈 쉐이더 프로그램 생성

	if (ShaderProgram == 0) { //쉐이더 프로그램이 만들어졌는지 확인
		fprintf(stderr, "Error creating shader program\n");
	}

	std::string vs, fs;

	//shader.vs 가 vs 안으로 로딩됨
	if (!ReadFile(filenameVS, &vs)) {
		printf("Error compiling vertex shader\n");
		return -1;
	};

	//shader.fs 가 fs 안으로 로딩됨
	if (!ReadFile(filenameFS, &fs)) {
		printf("Error compiling fragment shader\n");
		return -1;
	};

	// ShaderProgram 에 vs.c_str() 버텍스 쉐이더를 컴파일한 결과를 attach함
	AddShader(ShaderProgram, vs.c_str(), GL_VERTEX_SHADER);

	// ShaderProgram 에 fs.c_str() 프레그먼트 쉐이더를 컴파일한 결과를 attach함
	AddShader(ShaderProgram, fs.c_str(), GL_FRAGMENT_SHADER);

	GLint Success = 0;
	GLchar ErrorLog[1024] = { 0 };

	//Attach 완료된 shaderProgram 을 링킹함
	glLinkProgram(ShaderProgram);

	//링크가 성공했는지 확인
	glGetProgramiv(ShaderProgram, GL_LINK_STATUS, &Success);

	if (Success == 0) {
		// shader program 로그를 받아옴
		glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
		std::cout << filenameVS << ", " << filenameFS << " Error linking shader program\n" << ErrorLog;
		return -1;
	}

	glValidateProgram(ShaderProgram);
	glGetProgramiv(ShaderProgram, GL_VALIDATE_STATUS, &Success);
	if (!Success) {
		glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
		std::cout << filenameVS << ", " << filenameFS << " Error validating shader program\n" << ErrorLog;
		return -1;
	}

	glUseProgram(ShaderProgram);
	std::cout << filenameVS << ", " << filenameFS << " Shader compiling is done.\n";

	return ShaderProgram;
}
unsigned char * Renderer::loadBMPRaw(const char * imagepath, unsigned int& outWidth, unsigned int& outHeight)
{
	std::cout << "Loading bmp file " << imagepath << " ... " << std::endl;
	outWidth = -1;
	outHeight = -1;
	// Data read from the header of the BMP file
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	// Actual RGB data
	unsigned char * data;

	// Open the file
	FILE * file = NULL;
	fopen_s(&file, imagepath, "rb");
	if (!file)
	{
		std::cout << "Image could not be opened, " << imagepath << " is missing. " << std::endl;
		return NULL;
	}

	if (fread(header, 1, 54, file) != 54)
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	if (header[0] != 'B' || header[1] != 'M')
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	if (*(int*)&(header[0x1E]) != 0)
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	if (*(int*)&(header[0x1C]) != 24)
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	outWidth = *(int*)&(header[0x12]);
	outHeight = *(int*)&(header[0x16]);

	if (imageSize == 0)
		imageSize = outWidth * outHeight * 3;

	if (dataPos == 0)
		dataPos = 54;

	data = new unsigned char[imageSize];

	fread(data, 1, imageSize, file);

	fclose(file);

	std::cout << imagepath << " is succesfully loaded. " << std::endl;

	return data;
}

GLuint Renderer::CreatePngTexture(char * filePath)
{
	//Load Pngs: Load file and decode image.
	std::vector<unsigned char> image;
	unsigned width, height;
	unsigned error = lodepng::decode(image, width, height, filePath);
	if (error != 0)
	{
		lodepng_error_text(error);
		assert(error == 0);
		return -1;
	}

	GLuint temp;
	glGenTextures(1, &temp);

	glBindTexture(GL_TEXTURE_2D, temp);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);

	return temp;
}

GLuint Renderer::CreateBmpTexture(char * filePath)
{
	//Load Bmp: Load file and decode image.
	unsigned int width, height;
	unsigned char * bmp
		= loadBMPRaw(filePath, width, height);

	if (bmp == NULL)
	{
		std::cout << "Error while loading bmp file : " << filePath << std::endl;
		assert(bmp != NULL);
		return -1;
	}

	GLuint temp;
	glGenTextures(1, &temp);

	glBindTexture(GL_TEXTURE_2D, temp);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, bmp);

	return temp;
}

void Renderer::Test()
{
	/*Prepare step*/
	glUseProgram(m_SolidRectShader);

	// 쉐이더 다음시간에 설명함
	int attribPosition = glGetAttribLocation(m_SolidRectShader, "a_Position");
	glEnableVertexAttribArray(attribPosition);

	// VBO Bind
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTest); // m_VBOTest --> bind --> GL_ARRAY_BUFFER
	glVertexAttribPointer(attribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);

	/*Render*/
	glDrawArrays(GL_LINES, 0, 3);

	glDisableVertexAttribArray(attribPosition);
}

void Renderer::DrawSign()
{
	/*Prepare step*/
	glUseProgram(m_SolidRectShader);

	// 쉐이더 다음시간에 설명함
	int attribPosition = glGetAttribLocation(m_SolidRectShader, "a_Position");
	glEnableVertexAttribArray(attribPosition);

	// VBO Bind
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOInitialName); // m_VBOTest --> bind --> GL_ARRAY_BUFFER
	glVertexAttribPointer(attribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);

	/*Render*/

	// L
	glDrawArrays(GL_LINES, 0, 2);
	glDrawArrays(GL_LINES, 1, 2);

	// W
	glDrawArrays(GL_LINES, 3, 2);
	glDrawArrays(GL_LINES, 4, 2);
	glDrawArrays(GL_LINES, 5, 2);
	glDrawArrays(GL_LINES, 6, 2);

	// T
	glDrawArrays(GL_LINES, 8, 2);
	glDrawArrays(GL_LINES, 9, 2);
	glDrawArrays(GL_LINES, 10, 2);

	glDisableVertexAttribArray(attribPosition);
}

void Renderer::Lecture3(float elapsedTime)
{
	int vertexStride = 15;

	/*Prepare step*/
	GLuint shader = m_SolidRectShader;
	glUseProgram(shader);

	// alpha Blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// vs input을 gl에 알려주는 작업 : vs input(in vs) == C++ 에서 알려줘야하는 정보
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOManyParticle); // 18개 float

	GLuint posId = glGetAttribLocation(shader, "a_Position");
	glEnableVertexAttribArray(posId); //layout
	glVertexAttribPointer(posId, 3, GL_FLOAT, GL_FALSE, sizeof(float) * vertexStride, (GLvoid*)0);
	
	GLuint velId = glGetAttribLocation(shader, "a_Vel");
	glEnableVertexAttribArray(velId);
	glVertexAttribPointer(velId, 3, GL_FLOAT, GL_FALSE, sizeof(float) * vertexStride, (GLvoid*)(sizeof(float)*3));

	GLuint startTimeId = glGetAttribLocation(shader, "a_StartTime");
	glEnableVertexAttribArray(startTimeId);
	glVertexAttribPointer(startTimeId,1, GL_FLOAT, GL_FALSE, sizeof(float) * vertexStride, (GLvoid*)(sizeof(float) * 6));

	GLuint lifeTimeId = glGetAttribLocation(shader, "a_LifeTime");
	glEnableVertexAttribArray(lifeTimeId);
	glVertexAttribPointer(lifeTimeId, 1, GL_FLOAT, GL_FALSE, sizeof(float) * vertexStride, (GLvoid*)(sizeof(float) * 7));

	GLuint periodId = glGetAttribLocation(shader, "a_Period");
	glEnableVertexAttribArray(periodId);
	glVertexAttribPointer(periodId, 1, GL_FLOAT, GL_FALSE, sizeof(float) * vertexStride, (GLvoid*)(sizeof(float) * 8));

	GLuint ampId = glGetAttribLocation(shader, "a_Amp");
	glEnableVertexAttribArray(ampId);
	glVertexAttribPointer(ampId, 1, GL_FLOAT, GL_FALSE, sizeof(float) * vertexStride, (GLvoid*)(sizeof(float) * 9));

	GLuint valueId = glGetAttribLocation(shader, "a_Value");
	glEnableVertexAttribArray(valueId);
	glVertexAttribPointer(valueId, 1, GL_FLOAT, GL_FALSE, sizeof(float) * vertexStride, (GLvoid*)(sizeof(float) * 10));

	// Random Color
	GLuint colorId = glGetAttribLocation(shader, "a_Color");
	glEnableVertexAttribArray(colorId);
	// 4개를 읽어와라
	glVertexAttribPointer(colorId, 4, GL_FLOAT, GL_FALSE, sizeof(float) * vertexStride, (GLvoid*)(sizeof(float) * 11));

	GLuint timeID = glGetUniformLocation(shader, "u_Time");
	glUniform1f(timeID, m_TotalTimeLecture3);

	/*Render*/
	glDrawArrays(GL_TRIANGLES, 0, m_VBOManyParticleCount);

	m_TotalTimeLecture3 += elapsedTime;

	glDisable(GL_BLEND);
}

void Renderer::FSSandbox(float elapsedTime)
{
	m_TotalTimeFSSandBox += elapsedTime;

	int vertexStride = 3;
	int vertexCount = 6;

	/*Prepare step*/
	GLuint shader = m_FSSandboxShader;
	glUseProgram(shader);

	// alpha Blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	GLuint time = glGetUniformLocation(shader, "u_Time");
	glUniform1f(time, m_TotalTimeFSSandBox);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBOFSSandbox);
	GLuint posId = glGetAttribLocation(shader, "a_Position");

	glEnableVertexAttribArray(posId); //layout
	glVertexAttribPointer(posId, 3, GL_FLOAT, GL_FALSE, sizeof(float) * vertexStride, (GLvoid*)0);

	glDrawArrays(GL_TRIANGLES, 0, vertexCount);

	glDisable(GL_BLEND);
}
